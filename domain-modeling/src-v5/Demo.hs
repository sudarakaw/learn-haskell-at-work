{-# LANGUAGE OverloadedStrings #-}

module Demo (someProject) where

import           Data.Generics.Fixplate.Draw

import           PrettyPrint
import           Project
import           Reporting

someProject :: Project
someProject = projectGroup "Sweden" [stockholm, gothenburg, malmo]
  where
    stockholm = project 1 "Stockholm"
    gothenburg = project 2 "Gothenburg"
    malmo = projectGroup "Malmo" [city, limhamn]
    city = project 3 "Malmo City"
    limhamn = project 4 "Limhamn"
