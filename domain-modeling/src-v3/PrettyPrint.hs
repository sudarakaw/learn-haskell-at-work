module PrettyPrint where


import           Data.Decimal (roundTo)
import qualified Data.Text    as Text
import           Data.Tree
import           Text.Printf

import           Project
import           Reporting

asTree :: (a -> String) -> Project a -> Tree String
asTree f (Project name x)
  = Node (printf "%s: %s" name (f x)) []
asTree f (ProjectGroup name ps)
  = Node (Text.unpack name) (map (asTree f) ps)

prettyMoney :: Money ->String
prettyMoney (Money d) = sign ++ show (roundTo 2 d)
  where
    sign = if d > 0
              then "+"
              else ""

prettyProject :: (a -> String) -> Project a -> String
prettyProject f = drawTree . asTree f


prettyReport :: Report -> String
prettyReport r =
  printf
    "Budget: %s, Net: %s, Difference: %s"
    (prettyMoney (budgetProfit r))
    (prettyMoney (netProfit r))
    (prettyMoney (difference r))
