module PrettyPrint where


import qualified Data.Text   as Text
import           Data.Tree
import           Text.Printf

import           Project
import           Reporting

asTree :: (a -> String) -> Project a -> Tree String
asTree f (Project name x)
  = Node (printf "%s: %s" name (f x)) []
asTree f (ProjectGroup name ps)
  = Node (Text.unpack name) (map (asTree f) ps)

prettyProject :: (a -> String) -> Project a -> String
prettyProject f = drawTree . asTree f


prettyReport :: Report -> String
prettyReport r =
  printf
    "Budget: %.2f, Net: %.2f, Difference: %.2f"
    (unMoney (budgetProfit r))
    (unMoney (netProfit r))
    (unMoney (difference r))
