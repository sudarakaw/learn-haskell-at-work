{-# LANGUAGE DeriveTraversable          #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Project where

import           Data.Text        (Text)
import           Data.Traversable

newtype Money = Money { unMoney :: Double }
  deriving (Show, Eq, Num)

newtype ProjectId = ProjectId { unProjectId :: Int }
  deriving (Show, Eq, Num)

data Project a
  = Project Text a
  | ProjectGroup Text [Project a]
  deriving (Show, Eq, Functor, Foldable, Traversable)

data Budget = Budget
  { budgetIncome      :: Money
  , budgetExpenditure :: Money
  }
  deriving (Show, Eq)

data Transaction
  = Sale Money
  | Purchase Money
  deriving (Show, Eq)
