module SW.PluralizeSpec
  ( spec
  )
where


import           Control.Monad                  ( forM_ )
import           Data.Text                      ( Text )
import qualified Data.Text                     as Text
import qualified Data.Text.IO                  as Text
import           Test.Hspec
import           Text.Printf                    ( printf )

import           SW.Pluralize

readSampleData :: IO [(Text, Text)]
readSampleData = mapM asPair =<< Text.lines <$> Text.readFile
  "test/SW/plurals.csv"
 where
  asPair line = case Text.splitOn "," line of
    [input, expected] -> pure (input, expected)
    _                 -> fail $ "Invalid sample data: " <> Text.unpack line

spec :: Spec
spec = do
  examples <- runIO readSampleData

  forM_ examples $ \(input, expected) ->
    it (printf "pluralize '%s' to '%s'" input expected)
      $          pluralize input
      `shouldBe` expected
