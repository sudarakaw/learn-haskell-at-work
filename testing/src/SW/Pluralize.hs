module SW.Pluralize
  ( pluralize
  )
where

import           Data.Text                      ( Text )
import qualified Data.Text                     as Text

pluralize :: Text -> Text
pluralize ""       = ""
pluralize "fungus" = "fungi"
pluralize "schema" = "schemata"
pluralize t        = case Text.last t of
  's' -> t
  _   -> t <> "s"
