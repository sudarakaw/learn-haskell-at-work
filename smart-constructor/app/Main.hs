module Main where

import           Data.HashSet       (toList)
import           Data.String        (fromString)
import           Data.Text          (Text)
import qualified Data.Text          as T
import           System.Environment
import           System.IO
import           Text.Printf        (printf)

import           SerialNumber

getInput :: String -> IO Text
getInput prompt = do
  putStr $ prompt ++ ": "

  hFlush stdout

  fromString <$> getLine

(~~) :: String -> Text -> String
s ~~ t = s ++ printf "%s" t

main :: IO ()
main = do
  args <- getArgs

  input <- case args of
    [] -> getInput "Enter the serial number"
    _  ->
      let
        txt :: Text
        txt = fromString $ args >>= id
      in
        return txt

  let sn = makeSerialNumber input

  case sn of
    Right sn ->
      putStrLn $ "Serial number " ~~ (renderSerialNumber sn) ++ " is valid!"

    Left (InvalidGroupLength available expected text) ->
      putStrLn $ "Group \"" ~~ text ++ "\" have " ++ (show available) ++ " symbols. Need " ++ (show expected) ++ "."

    Left (InvalidCharacters text) ->
      putStrLn $ "Following characters are not acceptable: " ++ (toList text)

    Left (WrongNumberOfGroups available expected) ->
      putStrLn $ "Only " ++ (show available) ++ " groups given. Need " ++ (show expected) ++ "."

-- $> makeSerialNumber $ fromString ""

-- $> makeSerialNumber $ fromString "asd"

-- $> makeSerialNumber $ fromString "asdf"

-- $> makeSerialNumber $ fromString "ASDF"

-- $> makeSerialNumber $ fromString "ASDFG"

-- $> makeSerialNumber $ fromString "ASDF-123"

-- $> makeSerialNumber $ fromString "ASDF-123x-QWER"

-- $> makeSerialNumber $ fromString "ASDF-123X-QWER-ZXVB"
