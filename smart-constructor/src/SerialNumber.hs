module SerialNumber
  ( SerialNumber
  , ValidationError(..)
  , renderSerialNumber
  , makeSerialNumber
  ) where

import           Data.HashSet (HashSet)
import qualified Data.HashSet as HS
import           Data.Text    (Text)
import qualified Data.Text    as T

newtype SerialNumber = SerialNumber [Text]
  deriving (Eq, Show)

data ValidationError
  = WrongNumberOfGroups Int Int
  | InvalidGroupLength Int Int Text
  | InvalidCharacters (HashSet Char)
  deriving (Eq, Show)

validChars :: HashSet Char
validChars = HS.fromList $ ['A' .. 'Z'] ++ ['0' .. '9']

seperator :: Text
seperator = T.singleton '-'

groupCount :: Int
groupCount = 4

groupLength :: Int
groupLength = 4

makeSerialNumber :: Text -> Either ValidationError SerialNumber
makeSerialNumber txt = do
  groups <- mapM validateGroup $ T.splitOn seperator txt

  if groupCount == length groups
     then Right $ SerialNumber groups
     else Left $ WrongNumberOfGroups (length groups) groupCount

  where
    validateGroup group
      | len /= 4 = Left $ InvalidGroupLength len groupLength group
      | not (HS.null invalidChars) = Left $ InvalidCharacters invalidChars
      | otherwise = Right group
      where
        len = T.length group
        invalidChars = HS.difference (HS.fromList $ T.unpack group) validChars

renderSerialNumber :: SerialNumber -> Text
renderSerialNumber (SerialNumber gs) = T.intercalate seperator gs
